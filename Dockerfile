# Build stage
FROM maven:3.8.4-openjdk-17-slim AS build
WORKDIR /app
COPY src src
COPY pom.xml .
RUN mvn -f pom.xml clean package

# Package stage
FROM openjdk:17-jdk-slim
WORKDIR /app
COPY --from=build /app/target/*.jar app.jar
EXPOSE 3000
ENTRYPOINT ["java","-jar","app.jar"]