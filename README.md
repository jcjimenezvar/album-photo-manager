# Album Management Service

## Overview
This service is an album management application developed using **Java 17 and Spring Framework 3.2.1.** It provides an API for enriching and saving albums, along with retrieving album details.
## Features

- **OpenAPI 3.0.1 Integration**: Utilizes OpenAPI specification for clear and standardized API documentation.
-  **CRUD Operations**: Supports operations to create, and retrieve albums.

## Prerequisites

- Java 17
- Spring Framework 3.2.1
- Maven
- Docker

## Installation and Running the Application

1. **Clone the repository**
   `git clone https://gitlab.com/jcjimenezvar/album-photo-manager.git`
2. **Build the Application:**
   `./mvnw package` or `mvn package`
3. **Build Docker Image**:
   `docker build -t album-management-service .`
4. **Run the Container**:
   `docker run -p 3000:3000 album-management-service`

## API Endpoints

The service exposes the following endpoints:

1.  **Enrich and Save Albums**

    -   **POST** `/albums/enriched`
    -   Enriches album data and saves it.
2.  **Enrich Albums**

    -   **GET** `/albums/enrich`
    -   Retrieves enriched album data.
3.  **Get Albums**

    -   **GET** `/albums`
    -   Retrieves all albums.

[See more details and test](http://localhost:3000/api/v1/swagger-ui/index.html)

## Models

-   **Album**: Represents an album with properties `id`, `title`, and `photos`.
-   **Photo**: Represents a photo with properties `id`, `title`, `url`, `thumbnailUrl`, and `albumId`.

## H2 Database

To access the H2 Database Console while the application is running inside Docker:

1. Ensure that `application.properties` includes the following settings to enable the H2 console and allow remote connections:
   ```properties
   spring.h2.console.enabled=true
   spring.h2.console.settings.web-allow-others=true

2. After starting the Docker container, navigate to: http://localhost:3000/h2-console
3. Log in with the appropriate JDBC URL, username, and password as configured for your H2 database.

Note: Allowing remote connections to the H2 console can pose a security risk. It's recommended to use this only in a development environment.

## Docker Deployment

The service is Docker-ready. Use the provided `Dockerfile` to build and run the application in a Docker container.


