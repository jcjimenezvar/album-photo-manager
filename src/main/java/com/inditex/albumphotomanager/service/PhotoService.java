package com.inditex.albumphotomanager.service;

import com.inditex.albumphotomanager.model.Photo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhotoService {
    private final DataService dataService;

    @Autowired
    public PhotoService(DataService dataService){
        this.dataService = dataService;
    }
    public List<Photo> getPhotos(){
        return this.dataService.getData("photos.url", Photo[].class);
    }
}
