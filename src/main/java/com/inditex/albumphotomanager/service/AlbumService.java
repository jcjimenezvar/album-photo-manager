package com.inditex.albumphotomanager.service;

import com.inditex.albumphotomanager.model.Album;
import com.inditex.albumphotomanager.model.Photo;
import com.inditex.albumphotomanager.repository.AlbumRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AlbumService {
    private final DataService dataService;
    private final PhotoService photoService;
    private final AlbumRepository albumRepository;

    @Autowired
    public AlbumService(DataService dataService, PhotoService photoService, AlbumRepository albumRepository) {
        this.dataService = dataService;
        this.photoService = photoService;
        this.albumRepository = albumRepository;
    }
    @Transactional
    public List<Album> enrichAndSave() {
        List<Album> albums = this.getAlbumsAndPhotos();
        this.albumRepository.saveAll(albums);
        return albums;
    }

    public List<Album> enrich() {
        return this.getAlbumsAndPhotos();
    }

    public List<Album> getAlbums() {
        return this.albumRepository.findAll();
    }

    private List<Album> getAlbumsAndPhotos() {

        List<Album> albums = this.dataService.getData("albums.url", Album[].class);
        List<Photo> photos = this.photoService.getPhotos();
        return linkPhotosToAlbums(albums, photos);
    }

    private List<Album> linkPhotosToAlbums(List<Album> albums, List<Photo> photos) {
        Map<Integer, List<Photo>> photoMap = photos.stream()
                .collect(Collectors.groupingBy(Photo::getAlbumId));

        albums.forEach(album -> {
            List<Photo> photos1 = photoMap.getOrDefault(album.getId(), new ArrayList<>());
            album.setPhotos(photos1);
            photos1.forEach(photo -> photo.setAlbum(album));
        });
        return albums;
    }
}
