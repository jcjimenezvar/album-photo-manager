package com.inditex.albumphotomanager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class DataService {

    private final Environment env;
    private final RestTemplate restTemplate;

    @Autowired
    public DataService(Environment env, RestTemplate restTemplate) {
        this.env = env;
        this.restTemplate = restTemplate;
    }

    public <T> List<T> getData(String url, Class<T[]> clazz) {
        String resolvedUrl = Objects.requireNonNull(env.getProperty(url), "URL not found in environment properties");
        ResponseEntity<T[]> response = restTemplate.getForEntity(resolvedUrl, clazz);
        return Arrays.asList(Objects.requireNonNull(response.getBody()));
    }
}
