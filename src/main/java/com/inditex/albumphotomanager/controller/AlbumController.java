package com.inditex.albumphotomanager.controller;

import com.inditex.albumphotomanager.controller.handler.GlobalExceptionHandler;
import com.inditex.albumphotomanager.model.Album;
import com.inditex.albumphotomanager.service.AlbumService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/albums") // Versioning and base path
public class AlbumController {

    private final AlbumService albumService;

    @Autowired
    public AlbumController(AlbumService albumService) {
        this.albumService = albumService;
    }

    @Operation(summary = "Enrich and save albums", description = "Enriches albums with additional data saves and return them")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Album.class))}),
            @ApiResponse(responseCode = "404", description = "Resource not found",
                    content = @Content)})
    @PostMapping("/enriched")
    public ResponseEntity<List<Album>> enrichAndSaveAlbums() throws GlobalExceptionHandler {
        List<Album> enrichedAlbums = albumService.enrichAndSave();
        return new ResponseEntity<>(enrichedAlbums, HttpStatus.CREATED);
    }

    @Operation(summary = "Enrich albums", description = "Enriches albums with additional data and return them")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Album.class))}),
            @ApiResponse(responseCode = "404", description = "Resource not found",
                    content = @Content)})
    @GetMapping("/enrich")
    public ResponseEntity<List<Album>> enrichAlbums() throws GlobalExceptionHandler {
        List<Album> enrichedAlbums = albumService.enrich();
        return enrichedAlbums.isEmpty() ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) :
                new ResponseEntity<>(enrichedAlbums, HttpStatus.OK);
    }

    @Operation(summary = "Get albums", description = "Get albums from endpoint")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Album.class))}),
            @ApiResponse(responseCode = "404", description = "Resource not found",
                    content = @Content)})
    @GetMapping("")
    public ResponseEntity<List<Album>> getAlbums() throws GlobalExceptionHandler {
        List<Album> albums = albumService.getAlbums();
        return albums.isEmpty() ?
                new ResponseEntity<>(HttpStatus.NOT_FOUND) :
                new ResponseEntity<>(albums, HttpStatus.OK);
    }
}
