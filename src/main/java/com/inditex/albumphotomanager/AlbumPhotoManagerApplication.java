package com.inditex.albumphotomanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlbumPhotoManagerApplication {
	public static void main(String[] args) {
		SpringApplication.run(AlbumPhotoManagerApplication.class, args);
	}

}
