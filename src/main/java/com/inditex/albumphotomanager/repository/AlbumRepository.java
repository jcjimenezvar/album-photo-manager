package com.inditex.albumphotomanager.repository;


import com.inditex.albumphotomanager.model.Album;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlbumRepository extends JpaRepository<Album, Long> {
}
