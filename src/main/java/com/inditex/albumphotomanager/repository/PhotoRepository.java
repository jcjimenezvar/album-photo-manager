package com.inditex.albumphotomanager.repository;

import com.inditex.albumphotomanager.model.Photo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhotoRepository extends JpaRepository<Photo, Long> {

}
