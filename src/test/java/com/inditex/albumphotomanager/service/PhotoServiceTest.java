package com.inditex.albumphotomanager.service;

import com.inditex.albumphotomanager.UtilTest;
import com.inditex.albumphotomanager.model.Album;
import com.inditex.albumphotomanager.model.Photo;
import net.datafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PhotoServiceTest {

    private List<Album> albums = new ArrayList<>();
    private List<Photo> photos = new ArrayList<>();
    final Faker faker = new Faker();
    @Mock
    private DataService dataService;
    @InjectMocks
    private PhotoService photoService;

    @Mock
    private UtilTest utilTest;

    @BeforeEach
    void setUp() {
        albums.clear();  // Clear the albums list
        photos.clear();  // Clear the photos list
        utilTest = new UtilTest();
        // Initialize albums and photos
        albums = utilTest.getAlbums(faker);
        photos = utilTest.getPhotos(faker, albums);
        when(dataService.getData("photos.url", Photo[].class)).thenReturn(photos);
    }

    @Test
    void getPhotos() {
        // Act
        List<Photo> photosData = photoService.getPhotos();

        // Assert
        photosData.forEach(photo -> {
            assertTrue(photo.getId() > 0, "Photo ID should be greater than 0");
            assertNotNull(photo.getTitle(), "Photo title should not be null");
            assertNotNull(photo.getUrl(), "Photo url should not be null");
            assertNotNull(photo.getThumbnailUrl(), "Photo thumbnailUrl should not be null");
            assertTrue(photo.getAlbumId() > 0 , "Photo album id should not be zero");
            assertNotNull(photo.getAlbum(), "Photo album should not be null");
            assertTrue(albums.get(photo.getAlbumId()-1).getId() > 0, "Photo album should not be null");

        });
        assertEquals(photosData.size(), photos.size(), "The number of photos should match the expected size.");
    }
}
