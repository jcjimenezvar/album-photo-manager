package com.inditex.albumphotomanager.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

class DataServiceTest {

    @Mock
    private Environment env;

    @Mock
    private RestTemplate restTemplate;

    @Mock
    private RestTemplateBuilder restTemplateBuilder;

    @InjectMocks
    private DataService dataService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        when(env.getProperty(anyString())).thenReturn("http://example.com/api/data");
        when(restTemplateBuilder.build()).thenReturn(restTemplate);
        when(restTemplate.getForEntity(anyString(), any())).thenReturn(ResponseEntity.ok(new String[]{"data1", "data2"}));
    }

    @Test
    void testGetData() {
        List<String> result = dataService.getData("api.url", String[].class);
        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals("data1", result.get(0));
        assertEquals("data2", result.get(1));
    }

    // Additional tests for error scenarios
}
