package com.inditex.albumphotomanager.service;

import com.inditex.albumphotomanager.UtilTest;
import com.inditex.albumphotomanager.controller.handler.GlobalExceptionHandler;
import com.inditex.albumphotomanager.model.Album;
import com.inditex.albumphotomanager.model.Photo;
import com.inditex.albumphotomanager.repository.AlbumRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import net.datafaker.Faker;

@ExtendWith(MockitoExtension.class)
public class AlbumServiceTest {
    private List<Album> albums = new ArrayList<>();
    private List<Photo> photos = new ArrayList<>();
    final Faker faker = new Faker();
    @Mock
    private DataService dataService;
    @Mock
    private PhotoService photoService;
    @InjectMocks
    private AlbumService albumService;
    @Mock
    private AlbumRepository albumRepository;

    @Mock
    private UtilTest utilTest;
    @BeforeEach
    void setUp() {
        albums.clear();  // Clear the albums list
        photos.clear();  // Clear the photos list
        albumRepository.deleteAll();
        utilTest = new UtilTest();
        // Initialize albums and photos
        albums = utilTest.getAlbums(faker);
        photos = utilTest.getPhotos(faker, albums);
        lenient().when(dataService.getData("albums.url", Album[].class)).thenReturn(albums);
        lenient().when(photoService.getPhotos()).thenReturn(photos);
    }

    @Test
    void enrichAndSaveTest() throws GlobalExceptionHandler {
        // Act
        List<Album> enrichedAlbums = albumService.enrichAndSave();
        // Mock the behavior of external services
        when(albumRepository.findAll()).thenReturn(albums); // Mocking the response

        // Assert
        assertEquals(albums.size(), enrichedAlbums.size(), "The number of albums should match the expected size.");

        enrichedAlbums.forEach(album -> {
            assertTrue(album.getId() > 0, "Album ID should be greater than 0");
            assertNotNull(album.getTitle(), "Album title should not be null");

            assertFalse(album.getPhotos().isEmpty(), "Photo list for album ID " + album.getId() + " should not be empty");
            album.getPhotos().forEach(photo -> assertEquals(album.getId(), photo.getAlbumId(), "Photo's album ID should match the album's ID for album ID " + album.getId()));
        });
        assertEquals(albumRepository.findAll().size(), enrichedAlbums.size(), "The number of saved albums should match the expected size.");
    }

    @Test
    void enrich() throws GlobalExceptionHandler {
        // Act
        List<Album> enrichedAlbums = albumService.enrich();

        // Assert
        assertEquals(albums.size(), enrichedAlbums.size(), "The number of albums should match the expected size.");
        enrichedAlbums.forEach(album -> {
            assertTrue(album.getId() > 0, "Album ID should be greater than 0");
            assertNotNull(album.getTitle(), "Album title should not be null");

            assertFalse(album.getPhotos().isEmpty(), "Photo list for album ID " + album.getId() + " should not be empty");
            album.getPhotos().forEach(photo -> assertEquals(album.getId(), photo.getAlbumId(), "Photo's album ID should match the album's ID for album ID " + album.getId()));
        });
    }

    @Test
    void getAlbums() throws GlobalExceptionHandler {
        // Act
        List<Album> enrichedAlbums = albumService.getAlbums();
        when(albumRepository.findAll()).thenReturn(albums); // Mocking the response

        // Assert
        List<Album> albumsFromDB = albumRepository.findAll();
        assertEquals(albumsFromDB.size(), albums.size(), "The number of albums should match the expected size.");
        albumsFromDB.forEach(album -> {
            assertTrue(album.getId() > 0, "Album ID should be greater than 0");
            assertNotNull(album.getTitle(), "Album title should not be null");

            assertFalse(album.getPhotos().isEmpty(), "Photo list for album ID " + album.getId() + " should not be empty");
            album.getPhotos().forEach(photo -> assertEquals(album.getId(), photo.getAlbumId(), "Photo's album ID should match the album's ID for album ID " + album.getId()));
        });
    }

}
