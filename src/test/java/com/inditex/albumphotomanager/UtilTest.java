package com.inditex.albumphotomanager;

import com.inditex.albumphotomanager.model.Album;
import com.inditex.albumphotomanager.model.Photo;
import net.datafaker.Faker;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UtilTest {
    private final List<Album> albums = new ArrayList<>();
    private final List<Photo> photos = new ArrayList<>();

    public List<Album> getAlbums(Faker faker){
        int albumRandom = faker.random().nextInt(1, 10);
        for (int i = 0; i < albumRandom; i++) {
            Album album = new Album();
            album.setTitle(faker.lorem().word());
            album.setId(i + 1); // Set unique ID
            albums.add(album);
        }
        return albums;
    }
    public List<Photo> getPhotos(Faker faker, List<Album> albums) {
        int random = faker.random().nextInt(1, 100);
        for (int i = 0; i < random; i++) {
            Photo photo = new Photo();
            photo.setId(faker.number().numberBetween(1, 100));
            photo.setTitle(faker.lorem().word());
            photo.setUrl(faker.internet().url());
            photo.setThumbnailUrl(faker.internet().url());

            // Evenly distribute photos across albums
            int albumIndex = i % albums.size();
            Album album = albums.get(albumIndex);
            photo.setAlbumId(album.getId());
            photo.setAlbum(album);
            photos.add(photo);
        }

        // Associate photos with albums
        albums.forEach(album -> {
            List<Photo> albumPhotos = photos.stream()
                    .filter(p -> p.getAlbumId() == album.getId())
                    .collect(Collectors.toList());
            album.setPhotos(albumPhotos);
        });

        return photos;
    }
}
