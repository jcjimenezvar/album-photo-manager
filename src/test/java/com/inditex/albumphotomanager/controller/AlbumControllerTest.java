package com.inditex.albumphotomanager.controller;

import com.inditex.albumphotomanager.controller.handler.GlobalExceptionHandler;
import com.inditex.albumphotomanager.model.Album;
import com.inditex.albumphotomanager.service.AlbumService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
public class AlbumControllerTest {

    @Mock
    private AlbumService albumService;

    @InjectMocks
    private AlbumController albumController;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(albumController)
                .setControllerAdvice(new GlobalExceptionHandler())
                .build();
    }

    @Test
    public void testEnrichAndSaveAlbums() throws Exception, GlobalExceptionHandler {
        List<Album> albums = Arrays.asList(new Album(), new Album());
        when(albumService.enrichAndSave()).thenReturn(albums);

        mockMvc.perform(post("/albums/enriched")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)));

        verify(albumService, times(1)).enrichAndSave();
    }

    @Test
    public void testEnrichAlbums() throws Exception, GlobalExceptionHandler {
        List<Album> albums = Collections.singletonList(new Album());
        when(albumService.enrich()).thenReturn(albums);

        mockMvc.perform(get("/albums/enrich")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)));

        verify(albumService, times(1)).enrich();
    }

    @Test
    public void testGetAlbums() throws Exception, GlobalExceptionHandler {
        when(albumService.getAlbums()).thenReturn(Collections.emptyList());

        mockMvc.perform(get("/albums")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(albumService, times(1)).getAlbums();
    }

    // Additional test cases for error scenarios and edge cases can be added here.
}
